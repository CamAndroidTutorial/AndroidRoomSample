# Android ROOM Library Demo

You need to be solidly familiar with the Java programming language, object-oriented design concepts, and Android Development Fundamentals. In particular:

- RecyclerView and Adapters
- SQLite database and the SQLite query language
- Threadig and AsyncTask
- It helps to be familiar with software architectural patterns that separate data from the user interface, such as MVP or MVC. 
