package com.bunhann.androidroomsample;

import android.arch.persistence.db.SupportSQLiteDatabase;
import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import android.os.AsyncTask;
import android.support.annotation.NonNull;

import com.bunhann.androidroomsample.dao.NoteDAO;
import com.bunhann.androidroomsample.model.Note;

@Database(entities = {Note.class}, version = 1, exportSchema = false)
public abstract class NoteRoomDatabase extends RoomDatabase{

    public abstract NoteDAO noteDAO();

    private static NoteRoomDatabase INSTANCE;

    public static NoteRoomDatabase getDatabase(final Context context){
        if (INSTANCE == null ){
            synchronized (NoteRoomDatabase.class){
                if (INSTANCE == null ){
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            NoteRoomDatabase.class, "note_db")
                            .fallbackToDestructiveMigration()
                            .addCallback(sRoomDatabaseCallback)
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback = new RoomDatabase.Callback(){
        @Override
        public void onOpen(@NonNull SupportSQLiteDatabase db) {
            super.onOpen(db);
            //new PopulateDbAsync(INSTANCE).execute();
        }
    };


    private static class PopulateDbAsync extends AsyncTask<Void, Void, Void>{

        private final NoteDAO noteDao;

        public PopulateDbAsync(NoteRoomDatabase db) {
            noteDao = db.noteDAO();
        }


        @Override
        protected Void doInBackground(Void... voids) {
            //noteDao.deleteAll();
            Note note = new Note("My First Note");
            noteDao.insert(note);
            note = new Note("My Second Note");
            noteDao.insert(note);
            return null;
        }
    }

}
