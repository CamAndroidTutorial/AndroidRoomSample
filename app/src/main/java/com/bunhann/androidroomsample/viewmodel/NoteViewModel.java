package com.bunhann.androidroomsample.viewmodel;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import android.support.annotation.NonNull;

import com.bunhann.androidroomsample.model.Note;
import com.bunhann.androidroomsample.repository.NoteRepository;

import java.util.List;

public class NoteViewModel extends AndroidViewModel {
    private NoteRepository mRepository;
    private LiveData<List<Note>> allNotes;


    public NoteViewModel(@NonNull Application application) {
        super(application);
        mRepository = new NoteRepository(application);
        allNotes = mRepository.getAllNotes();
    }

    public LiveData<List<Note>> getAllNotes() {
        return allNotes;
    }

    public void setAllNotes(LiveData<List<Note>> allNotes) {
        this.allNotes = allNotes;
    }

    public void insert(Note note) {
        mRepository.insert(note);
    }

    public void deleteAll() {
        mRepository.deleteAll();
    }
}
