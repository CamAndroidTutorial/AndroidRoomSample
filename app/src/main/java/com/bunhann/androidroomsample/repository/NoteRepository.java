package com.bunhann.androidroomsample.repository;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.bunhann.androidroomsample.NoteRoomDatabase;
import com.bunhann.androidroomsample.dao.NoteDAO;
import com.bunhann.androidroomsample.model.Note;

import java.util.List;

public class NoteRepository {

    private NoteDAO mNoteDao;
    private LiveData<List<Note>> mAllNotes;

    public NoteRepository(Application application) {
        NoteRoomDatabase db = NoteRoomDatabase.getDatabase(application);
        mNoteDao = db.noteDAO();
        mAllNotes = mNoteDao.getAllNotes();

    }

    public LiveData<List<Note>> getAllNotes() {
        return mAllNotes;
    }

    public void insert (Note note){
        new insertAsyncTask(mNoteDao).execute(note);
    }

    private static class insertAsyncTask extends AsyncTask<Note, Void, Void>{

        private NoteDAO mAsyncTaskDao;

        public insertAsyncTask(NoteDAO mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected Void doInBackground(Note... notes) {
            mAsyncTaskDao.insert(notes[0]);
            return null;
        }
    }

    public static class deleteAllAsyncTask extends AsyncTask<Void, Void, Void>{
        private NoteDAO mAsyncTaskDao;

        public deleteAllAsyncTask(NoteDAO mAsyncTaskDao) {
            this.mAsyncTaskDao = mAsyncTaskDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }

    public void deleteAll(){
        new deleteAllAsyncTask(mNoteDao).execute();
    }

}
