package com.bunhann.androidroomsample.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bunhann.androidroomsample.R;
import com.bunhann.androidroomsample.model.Note;

import java.util.List;

public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.NoteViewHolder> {

    private Context context;
    private List<Note> mNotes;
    private LayoutInflater inflater;

    public NoteListAdapter(Context context) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @NonNull
    @Override
    public NoteViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View convertView = inflater.inflate(R.layout.recyclerview_item, parent, false);
        NoteViewHolder noteViewHolder = new NoteViewHolder(convertView);
        return noteViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder holder, int position) {
        if (mNotes != null) {
            Note current = mNotes.get(position);
            holder.noteItemView.setText(current.getWord());
        } else {
            holder.noteItemView.setText("No Note");
        }
    }
    public void setNotes (List<Note> notes){
        mNotes = notes;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mNotes !=null){
            return mNotes.size();
        } else return 0;
    }

    class NoteViewHolder extends RecyclerView.ViewHolder{
        private final TextView noteItemView;

        public NoteViewHolder(View itemView) {
            super(itemView);
            this.noteItemView = (TextView) itemView.findViewById(R.id.textView);
        }
    }
}
