package com.bunhann.androidroomsample;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewNoteActivity extends AppCompatActivity {

    public static final String REPLY_EXTRA = "com.bunhann.new_note";

    private EditText editNote;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        editNote = (EditText) findViewById(R.id.edit_note);
        final Button btnSave = (Button) findViewById(R.id.button_save);
        btnSave.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homeIntent = new Intent();
                if (TextUtils.isEmpty(editNote.getText())){
                    setResult(RESULT_CANCELED, homeIntent);
                } else {
                    String note = editNote.getText().toString();
                    homeIntent.putExtra(REPLY_EXTRA, note);
                    setResult(RESULT_OK, homeIntent);
                }
                finish();
            }
        });

    }
}
